<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;

class Report extends Model
{
    use SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'generated_at' => 'datetime',
        'results' => 'array',
    ];

    /**
     * @var array
     */
    public $categories = [
        'seo' => ['title', 'meta_description', 'headings', 'content_keywords', 'image_keywords', 'seo_friendly_url', '404_page', 'robots', 'noindex', 'in_page_links', 'language', 'favicon'],
        'performance' => ['text_compression', 'load_time', 'page_size', 'http_requests', 'image_format', 'defer_javascript', 'dom_size'],
        'security' => ['https_encryption', 'mixed_content', 'server_signature', 'unsafe_cross_origin_links', 'plaintext_email'],
        'miscellaneous' => ['structured_data', 'meta_viewport', 'charset', 'sitemap', 'social', 'content_length', 'text_html_ratio', 'inline_css', 'deprecated_html_tags'],
    ];

    /**
     * Scope a query to search by url.
     */
    public function scopeSearchUrl(Builder $query, string $value): Builder
    {
        return $query->where('url', 'like', '%'.cleanUrl($value).'%');
    }

    /**
     * Scope a query to search by project.
     */
    public function scopeSearchProject(Builder $query, string $value): Builder
    {
        return $query->where('project', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to search by user.
     */
    public function scopeOfUser(Builder $query, string $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    /**
     * Scope a query to search by project.
     */
    public function scopeOfProject(Builder $query, string $value): Builder
    {
        return $query->where('project', '=', $value);
    }

    /**
     * Scope a query to search by result.
     */
    public function scopeOfResult(Builder $query, string $value): Builder
    {
        if ($value == 'good') {
            return $query->where('result', '>', 79);
        }
        if ($value == 'decent') {
            return $query->where([['result', '>=', 49], ['result', '<=', 79]]);
        }

        return $query->where('result', '<', 49);
    }

    /**
     * Get the user that owns the Link.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the total score.
     */
    protected function totalScore(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $points = 0;
                foreach ($this->results as $key => $value) {
                    $points += config('settings.report_score_'.$value['importance']);
                }

                return $points;
            },
        );
    }

    /**
     * Get the score.
     */
    protected function score(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $points = 0;
                foreach ($this->results as $key => $value) {
                    if ($value['passed']) {
                        $points += config('settings.report_score_'.$value['importance']);
                    }
                }

                return $points;
            },
        );
    }

    /**
     * Get the issues count.
     */
    protected function highIssuesCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    if (! $value['passed'] && $value['importance'] == 'high') {
                        $count += 1;
                    }
                }

                return $count;
            },
        );
    }

    protected function mediumIssuesCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    if (! $value['passed'] && $value['importance'] == 'medium') {
                        $count += 1;
                    }
                }

                return $count;
            },
        );
    }

    /**
     * Get the low issues count.
     */
    protected function lowIssuesCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    if (! $value['passed'] && $value['importance'] == 'low') {
                        $count += 1;
                    }
                }

                return $count;
            },
        );
    }

    /**
     * Get the non issues count.
     */
    protected function nonIssuesCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    if ($value['passed']) {
                        $count += 1;
                    }
                }

                return $count;
            },
        );
    }

    /**
     * Get the total tests count.
     */
    protected function totalTestsCount(): Attribute
    {
        return new Attribute(
            get: fn ($value) => count($this->results),
        );
    }

    /**
     * Get the high issues seo count.
     */
    protected function highIssuesSeoCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['seo'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'high')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Get the high issues performance count.
     */
    protected function highIssuesPerformanceCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['performance'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'high')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Get the high issues security count.
     */
    protected function highIssuesSecurityCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['security'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'high')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Get the high issues accessibility count.
     */
    protected function highIssuesMiscellaneousCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['miscellaneous'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'high')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Get the medium issues seo count.
     */
    protected function mediumIssuesSeoCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['seo'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'medium')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Medium issues performance count.
     */
    protected function mediumIssuesPerformanceCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['performance'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'medium')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Medium issues security count.
     */
    protected function mediumIssuesSecurityCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['security'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'medium')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Medium issues miscellaneous count.
     */
    protected function mediumIssuesMiscellaneousCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['miscellaneous'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'medium')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Low issues seo count.
     */
    protected function lowIssuesSeoCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['seo'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'low')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Low issues performance count.
     */
    protected function lowIssuesPerformanceCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['performance'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'low')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Low issues security count.
     */
    protected function lowIssuesSecurityCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['security'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'low')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Low issues miscellaneous count.
     */
    protected function lowIssuesMiscellaneousCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $count = 0;
                foreach ($this->results as $key => $value) {
                    // If the result key exists under a category
                    if (! in_array($key, $this->categories['miscellaneous'])) {
                        continue;
                    }
                    if ($value['passed']) {
                        continue;
                    }
                    if (! ($value['importance'] == 'low')) {
                        continue;
                    }
                    $count += 1;
                }

                return $count;
            },
        );
    }

    /**
     * Categories.
     */
    protected function categories(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->categories,
        );
    }

    /**
     * Full url.
     */
    protected function fullUrl(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this['results']['seo_friendly_url']['value'],
        );
    }

    /**
     * Url.
     */
    protected function url(): Attribute
    {
        return new Attribute(
            set: fn ($value) => cleanUrl($value),
        );
    }

    /**
     * Project.
     */
    protected function project(): Attribute
    {
        return new Attribute(
            set: fn ($value) => parse_url(str_replace(['https://www.', 'http://www.'], ['https://', 'http://'], $value), PHP_URL_HOST),
        );
    }

    /**
     * Host.
     */
    protected function host(): Attribute
    {
        return new Attribute(
            get: fn ($value) => parse_url('http://'.$this->url, PHP_URL_HOST),
        );
    }

    /**
     * Password.
     */
    protected function password(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                try {
                    return Crypt::decryptString($value);
                } catch (\Exception $e) {
                    return null;
                }
            },
            set: fn ($value) => Crypt::encryptString($value),
        );
    }
}
