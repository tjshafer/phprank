<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'product' => 'object',
        'tax_rates' => 'object',
        'coupon' => 'object',
        'customer' => 'object',
        'seller' => 'object',
    ];

    /**
     * Get the user that owns the payment.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the plan of the payment.
     */
    public function plan(): BelongsTo
    {
        return $this->belongsTo(Plan::class)->withTrashed();
    }

    /**
     * Scope a query to search by payment id.
     */
    public function scopeSearchPayment(Builder $query, string $value): Builder
    {
        return $query->where('payment_id', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to search by invoice id.
     */
    public function scopeSearchInvoice(Builder $query, string $value): Builder
    {
        return $query->where([['invoice_id', 'like', '%'.$value.'%'], ['status', '=', 'completed']]);
    }

    /**
     * Scope a query to search by customer id.
     */
    public function scopeOfPlan(Builder $query, string $value): Builder
    {
        return $query->where('plan_id', '=', $value);
    }

    /**
     * Scope a query to search by customer id.
     */
    public function scopeOfUser(Builder $query, string $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    /**
     * Scope a query to search by customer id.
     */
    public function scopeOfInterval(Builder $query, string $value): Builder
    {
        return $query->where('interval', '=', $value);
    }

    /**
     * Scope a query to search by processor
     */
    public function scopeOfProcessor(Builder $query, string $value): Builder
    {
        return $query->where('processor', '=', $value);
    }

    /**
     * Scope a query to search by status
     */
    public function scopeOfStatus(Builder $query, string $value): Builder
    {
        return $query->where('status', '=', $value);
    }
}
