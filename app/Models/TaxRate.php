<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxRate extends Model
{
    use SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'regions' => 'object',
    ];

    /**
     * Scope a query to search by name.
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to search by type.
     */
    public function scopeOfType(Builder $query, string $value): Builder
    {
        return $query->where('type', '=', $value);
    }

    /**
     * Scope a query to search by region.
     */
    public function scopeOfRegion(Builder $query, string $value): Builder
    {
        $query->whereNull('regions')
            ->when($value, function ($query) use ($value) {
                $query->orWhere('regions', 'like', '%'.$value.'%');
            });

        return $query;
    }
}
