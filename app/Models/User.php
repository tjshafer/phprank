<?php

namespace App\Models;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use ProtoneMedia\LaravelVerifyNewEmail\MustVerifyNewEmail;

class User extends Authenticatable implements MustVerifyEmail, hasLocalePreference
{
    use MustVerifyNewEmail, Notifiable, SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'plan_created_at' => 'datetime',
        'plan_recurring_at' => 'datetime',
        'plan_ends_at' => 'datetime',
        'plan_trial_ends_at' => 'datetime',
        'tfa_code_created_at' => 'datetime',
        'billing_information' => 'object',
        'brand' => 'object',    ];

    /**
     * Scope a query to search by name.
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     *Scope a query to search by email.
     */
    public function scopeSearchEmail(Builder $query, string $value): Builder
    {
        return $query->where('email', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to search by role.
     */
    public function scopeOfRole(Builder $query, string $value): Builder
    {
        return $query->where('role', '=', $value);
    }

    /**
     * Get the preferred locale of the entity.
     */
    public function preferredLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * Returns whether the user is an admin or not.
     */
    public function admin(): bool
    {
        return $this->role == 1;
    }

    /**
     * Get the plan that the user owns.
     */
    public function plan(): BelongsTo
    {
        // If the current plan is default, or the plan is not active
        if ($this->planOnDefault() || ! $this->planActive()) {
            // Switch to the default plan
            $this->plan_id = 1;
        }

        return $this->belongsTo(Plan::class)->withTrashed();
    }

    /**
     * Determine if the plan subscription is no longer active.
     */
    public function planCancelled(): bool
    {
        return ! is_null($this->plan_ends_at);
    }

    /**
     * Determine if the plan subscription is within its trial period.
     */
    public function planOnTrial(): bool
    {
        return $this->plan_trial_ends_at && $this->plan_trial_ends_at->isFuture();
    }

    /**
     * Determine if the plan subscription is active.
     */
    public function planActive(): bool
    {
        if ($this->plan_payment_processor == 'paypal') {
            return $this->planOnTrial() || $this->planOnGracePeriod() || $this->plan_subscription_status == 'ACTIVE';
        }
        if ($this->plan_payment_processor == 'stripe') {
            return $this->planOnTrial() || $this->planOnGracePeriod() || $this->plan_subscription_status == 'active';
        } else {
            return ! $this->planCancelled() || $this->planOnTrial() || $this->planOnGracePeriod();
        }
    }

    /**
     * Determine if the plan subscription is recurring and not on trial.
     */
    public function planRecurring(): bool
    {
        return ! $this->planOnTrial() && ! $this->planCancelled();
    }

    /**
     * Determine if the plan subscription is within its grace period after cancellation.
     */
    public function planOnGracePeriod(): bool
    {
        return $this->plan_ends_at && $this->plan_ends_at->isFuture();
    }

    /**
     * Determine if the user is on the default plan subscription.
     */
    public function planOnDefault(): bool
    {
        return $this->plan_id == 1;
    }

    /**
     * Cancel the current plan.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function planSubscriptionCancel(): void
    {
        if ($this->plan_payment_processor == 'paypal') {
            $httpClient = new HttpClient();

            $httpBaseUrl = 'https://'.(config('settings.paypal_mode') == 'sandbox' ? 'api-m.sandbox' : 'api-m').'.paypal.com/';

            // Attempt to retrieve the auth token
            try {
                $payPalAuthRequest = $httpClient->request('POST', $httpBaseUrl.'v1/oauth2/token', [
                    'auth' => [config('settings.paypal_client_id'), config('settings.paypal_secret')],
                    'form_params' => [
                        'grant_type' => 'client_credentials',
                    ],
                ]
                );

                $payPalAuth = json_decode($payPalAuthRequest->getBody()->getContents());
            } catch (BadResponseException $e) {
            }

            // Attempt to cancel the subscription
            try {
                $payPalSubscriptionCancelRequest = $httpClient->request('POST', $httpBaseUrl.'v1/billing/subscriptions/'.$this->plan_subscription_id.'/cancel', [
                    'headers' => [
                        'Authorization' => 'Bearer '.$payPalAuth->access_token,
                        'Content-Type' => 'application/json',
                    ],
                    'body' => json_encode([
                        'reason' => __('Cancelled'),
                    ]),
                ]
                );
            } catch (BadResponseException $e) {
            }
        } elseif ($this->plan_payment_processor == 'stripe') {
            // Attempt to cancel the current subscription
            try {
                $stripe = new \Stripe\StripeClient(
                    config('settings.stripe_secret')
                );

                $stripe->subscriptions->update(
                    $this->plan_subscription_id,
                    ['cancel_at_period_end' => true]
                );
            } catch (\Exception $e) {
            }
        } elseif ($this->plan_payment_processor == 'razorpay') {
            // Attempt to cancel the current subscription
            try {
                $razorpay = new \Razorpay\Api\Api(config('settings.razorpay_key'), config('settings.razorpay_secret'));

                $razorpay->subscription->fetch($this->plan_subscription_id)->cancel();
            } catch (\Exception $e) {
            }
        } elseif ($this->plan_payment_processor == 'paystack') {
            $httpClient = new HttpClient();

            // Attempt to cancel the current subscription
            try {
                $paystackSubscriptionRequest = $httpClient->request('GET', 'https://api.paystack.co/subscription/'.$this->plan_subscription_id, [
                    'headers' => [
                        'Authorization' => 'Bearer '.config('settings.paystack_secret'),
                        'Content-Type' => 'application/json',
                        'Cache-Control' => 'no-cache',
                    ],
                ]
                );

                $paystackSubscription = json_decode($paystackSubscriptionRequest->getBody()->getContents());
            } catch (\Exception $e) {
            }

            if (isset($paystackSubscription->data->email_token)) {
                try {
                    $httpClient->request('POST', 'https://api.paystack.co/subscription/disable', [
                        'headers' => [
                            'Authorization' => 'Bearer '.config('settings.paystack_secret'),
                            'Content-Type' => 'application/json',
                            'Cache-Control' => 'no-cache',
                        ],
                        'body' => json_encode([
                            'code' => $this->plan_subscription_id,
                            'token' => $paystackSubscription->data->email_token,
                        ]),
                    ]
                    );
                } catch (\Exception $e) {
                }
            }
        }

        // Update the subscription end date and recurring date
        if (! empty($this->plan_recurring_at)) {
            $this->plan_ends_at = $this->plan_recurring_at;
            $this->plan_recurring_at = null;
        }
        $this->save();
    }

    /**
     * Get the user's reports.
     */
    public function reports(): HasMany
    {
        return $this->hasMany(Report::class)->where('user_id', $this->id);
    }

    /**
     * Get the user's reports count.
     */
    protected function reportsCount(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $now = Carbon::now();

                return $this->hasMany(\App\Models\Report::class)->withTrashed()->where('user_id', $this->id)->whereBetween('created_at', [clone $now->startOfMonth(), clone $now->endOfMonth()])->count();
            },
        );
    }
}
