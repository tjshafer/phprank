<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * {@inheritdoc}
     */
    public $fillable = [
        'name', 'slug', 'footer', 'content',
    ];

    /**
     * Scope a query to search by name.
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%')->orWhere('content', 'like', '%'.$value.'%');
    }
}
