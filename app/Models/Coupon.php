<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

    /**
     * Scope a query tby name
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query tbe code
     */
    public function scopeSearchCode(Builder $query, string $value): Builder
    {
        return $query->where('code', 'like', '%'.$value.'%');
    }

    /**
     *Scope a query to type
     */
    public function scopeOfType(Builder $query, string $value): Builder
    {
        return $query->where('type', '=', $value);
    }
}
