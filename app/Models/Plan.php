<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'items' => 'object',
        'tax_rates' => 'object',
        'coupons' => 'object',
        'features' => 'object',
    ];

    /**
     * Scope a query to search by name.
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to search by visibility.
     */
    public function scopeOfVisibility(Builder $query, string $value): Builder
    {
        return $query->where('visibility', '=', $value);
    }

    /**
     * Scope a query to search by price.
     */
    public function scopePriced(Builder $query): Builder
    {
        return $query->where([['amount_month', '>', 0], ['amount_year', '>', 0]]);
    }

    /**
     * Scope a query to search by price.
     */
    public function scopeDefault(Builder $query): Builder
    {
        return $query->where([['amount_month', '=', 0], ['amount_year', '=', 0]]);
    }

    /**
     * Get the plan price
     */
    public function hasPrice(): bool
    {
        return $this->amount_month || $this->amount_year;
    }
}
