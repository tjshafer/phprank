<?php

namespace App\Observers;

use App\Models\User;
use App\Traits\WebhookTrait;

class UserObserver
{
    use WebhookTrait;

    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        $this->callWebhook(config('settings.webhook_user_created'), [
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'action' => 'created',
        ]);
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        $this->callWebhook(config('settings.webhook_user_updated'), [
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'action' => 'updated',
        ]);
    }

    /**
     * Handle the User "forceDeleted" event.
     */
    public function forceDeleted(User $user): void
    {
        $this->callWebhook(config('settings.webhook_user_deleted'), [
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'action' => 'deleted',
        ]);
    }

    /**
     * Handle the User "deleting" event.
     */
    public function deleting(User $user): void
    {
        if ($user->isForceDeleting()) {
            // If the user previously had a subscription, attempt to cancel it
            if ($user->plan_subscription_id) {
                $user->planSubscriptionCancel();
            }

            $user->reports()->delete();
        } else {
            // If the user previously had a subscription, attempt to cancel it
            if ($user->plan_subscription_id) {
                $user->planSubscriptionCancel();
            }
        }
    }
}
