<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ReportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create reports.
     *
     * @return mixed
     */
    public function create(User $user): Response
    {
        if ($user->plan->features->reports <= 0) {
            return $this->allow();
        }
        if ($user->reportsCount < $user->plan->features->reports) {
            return $this->allow();
        }

        return $this->deny();
    }
}
