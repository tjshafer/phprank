<?php

/**
 * Format the page titles.
 */
function formatTitle(mixed $value = null): ?string
{
    if (is_array($value)) {
        return implode(' - ', $value);
    }

    return $value;
}

/**
 * Format money.
 */
function formatMoney(float $amount, string $currency, bool $separator = true, bool $translate = true): string
{
    if (in_array(strtoupper($currency), config('currencies.zero_decimals'))) {
        return number_format($amount, 0, $translate ? __('.') : '.', $separator ? ($translate ? __(',') : ',') : false);
    }

    return number_format($amount, 2, $translate ? __('.') : '.', $separator ? ($translate ? __(',') : ',') : false);
}

/**
 * Get and format the Gravatar URL.
 */
function gravatar(string $email, int $size = 80, string $default = 'identicon', string $rating = 'g'): string
{
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5(strtolower(trim($email)));
    $url .= '?s='.$size.'&d='.$default.'&r='.$rating;

    return $url;
}

/**
 * Format bytes to the metric system.
 */
function formatBytes(int $number, int $decimals, string $decimalSeparator, string $thousandsSeparator): string
{
    $units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    $power = $number > 0 ? floor(log($number, 1000)) : 0;

    return number_format($number / pow(1000, $power), $decimals, $decimalSeparator, $thousandsSeparator).' '.$units[$power];
}

/**
 * Create the container and format an array to HTML.
 */
function arrayToHtml(array $data, array $container = [], array $subContainer = [], array $rowContainer = [], array $keyContainer = [], array $valueContainer = []): void
{
    echo $container[0] ?? null;
    echo formatArrayToHtml($data, $subContainer, $rowContainer, $keyContainer, $valueContainer).($container[1] ?? null);
    echo $container[1] ?? null;
}

/**
 * Format a multi-dimensional array to HTML.
 */
function formatArrayToHtml(array $data, array $subContainer = [], array $rowContainer = [], array $keyContainer = [], array $valueContainer = []): void
{
    foreach ($data as $key => $value) {
        if (! is_array($value)) {
            echo($rowContainer[0] ?? null).($keyContainer[0] ?? null).e($key).($keyContainer[1] ?? null).($valueContainer[0] ?? null).e($value).($valueContainer[1] ?? null).($rowContainer[1] ?? null);
        } else {
            echo($rowContainer[0] ?? null).($keyContainer[0] ?? null).e($key).($keyContainer[1] ?? null).($subContainer[0] ?? null);
            formatArrayToHtml($value, $subContainer, $rowContainer, $keyContainer, $valueContainer);
            echo($subContainer[1] ?? null).($rowContainer[1] ?? null);
        }
    }
}

/**
 * Remove the http and www prefixes from an URL string.
 */
function cleanUrl(string $url): string
{
    return str_replace(['https://www.', 'http://www.', 'https://', 'http://'], '', (parse_url($url, PHP_URL_PATH) == '/' ? rtrim($url, '/') : $url));
}

/**
 * Convert a number into a readable one.
 */
function shortenNumber(mixed $number): string
{
    $suffix = ['', 'K', 'M', 'B'];
    $precision = 1;
    for ($i = 0; $i < count($suffix); $i++) {
        $divide = $number / pow(1000, $i);
        if ($divide < 1000) {
            return round($divide, $precision).$suffix[$i];
        }
    }

    return $number;
}
