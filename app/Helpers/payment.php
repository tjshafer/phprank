<?php

/**
 * Returns the discount amount.
 * Amount * Discount%
 */
function calculateDiscount(float $amount, float $discount): float|int
{
    return $amount * ($discount / 100);
}

/**
 * Returns the amount after discount.
 * Amount - Discount$
 */
function calculatePostDiscount(float $amount, float $discount): float|int
{
    return $amount - calculateDiscount($amount, $discount);
}

/**
 * Returns the inclusive taxes amount.
 * PostDiscount - PostDiscount / (1 + TaxRate)
 */
function calculateInclusiveTaxes(float $amount, float $discount, float $inclusiveTaxRate): float|int
{
    return calculatePostDiscount($amount, $discount) - (calculatePostDiscount($amount, $discount) / (1 + ($inclusiveTaxRate / 100)));
}

/**
 * Returns the amount after discount and included taxes.
 * PostDiscount - InclusiveTaxes$
 */
function calculatePostDiscountLessInclTaxes(float $amount, float $discount, float $inclusiveTaxRates): float|int
{
    return calculatePostDiscount($amount, $discount) - calculateInclusiveTaxes($amount, $discount, $inclusiveTaxRates);
}

/**
 * Returns the amount of an inclusive tax.
 * PostDiscountLessInclTaxes * (Tax / 100)
 */
function calculateInclusiveTax(float $amount, float $discount, float $inclusiveTaxRate, float $inclusiveTaxRates): float|int
{
    return calculatePostDiscountLessInclTaxes($amount, $discount, $inclusiveTaxRates) * ($inclusiveTaxRate / 100);
}

/**
 * Returns the exclusive tax amount.
 * PostDiscountLessInclTaxes * TaxRate
 */
function checkoutExclusiveTax(float $amount, float $discount, float $exclusiveTaxRate, float $inclusiveTaxRates): float|int
{
    return calculatePostDiscountLessInclTaxes($amount, $discount, $inclusiveTaxRates) * ($exclusiveTaxRate / 100);
}

/**
 * Calculate the total, including the exclusive taxes.
 * PostDiscount + ExclusiveTax$
 */
function checkoutTotal(float $amount, float $discount, float $exclusiveTaxRates, float $inclusiveTaxRates): float|int
{
    return calculatePostDiscount($amount, $discount) + checkoutExclusiveTax($amount, $discount, $exclusiveTaxRates, $inclusiveTaxRates);
}

/**
 * Get the enabled payment processors.
 */
function paymentProcessors(): array
{
    $paymentProcessors = config('payment.processors');

    foreach ($paymentProcessors as $key => $value) {
        // Check if the payment processor is not enabled
        if (! config('settings.'.$key)) {
            // Remove the payment processor from the list
            unset($paymentProcessors[$key]);
        }
    }

    return $paymentProcessors;
}
