<?php

namespace App\Traits;

use GuzzleHttp\Client as HttpClient;

trait WebhookTrait
{
    /**
     * Call webhook event.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function callWebhook(string $url, mixed $data)
    {
        if ($url) {
            try {
                $httpClient = new HttpClient();

                $httpClient->request('POST', $url, [
                    'timeout' => 5,
                    'form_params' => $data,
                ]);
            } catch (\Exception $e) {
            }
        }
    }
}
