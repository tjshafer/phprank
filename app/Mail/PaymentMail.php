<?php

namespace App\Mail;

use App\Models\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(public Payment $payment)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        if ($this->payment->status == 'completed') {
            $title = __('Payment completed');

            $data = [
                'introLines' => [__('The payment was successful.').' '.__('Thank you!')],
                'actionText' => __('Invoice'),
                'actionUrl' => route('account.invoices.show', [$this->payment->id]),
            ];
        } else {
            $title = __('Payment cancelled');

            $data = [
                'introLines' => [__('The payment was cancelled.')],
            ];
        }

        return $this->subject(formatTitle([$title, config('settings.title')]))
            ->markdown('vendor.notifications.email', $data);
    }
}
