<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->from(config('settings.email_address'))
            ->replyTo(request()->input('email'))
            ->subject(formatTitle([request()->input('subject'), config('settings.title')]))
            ->markdown('emails.contact', ['message' => request()->input('message')]);
    }
}
