<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateJsonStringRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (json_decode($value)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return 'Invalid JSON syntax.';
    }
}
