<?php

namespace App\Rules;

use App\Models\Report;
use Illuminate\Contracts\Validation\Rule;

class ValidateReportPasswordRule implements Rule
{
    public function __construct(private Report $report)
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if ($this->report->password == $value) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The entered password is not correct.');
    }
}
