<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class ValidateBadWordsRule implements Rule
{
    /**
     * The input attribute
     *
     * @var
     */
    private $attribute;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;

        $bannedWords = preg_split('/\n|\r/', config('settings.bad_words'), -1, PREG_SPLIT_NO_EMPTY);

        foreach ($bannedWords as $word) {
            // Search for the word in string
            if (Str::of(mb_strtolower($value))->contains(mb_strtolower($word))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The :attribute contains a keyword that is banned.', ['attribute' => $this->attribute]);
    }
}
