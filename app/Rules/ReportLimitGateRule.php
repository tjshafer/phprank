<?php

namespace App\Rules;

use App\Models\Report;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class ReportLimitGateRule implements Rule
{
    public function __construct(private User $user)
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->user->can('create', [Report::class])) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('You created too many reports.');
    }
}
