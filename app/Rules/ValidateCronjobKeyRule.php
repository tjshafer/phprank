<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateCronjobKeyRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if ($value == config('settings.cronjob_key')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Invalid cron job key.');
    }
}
