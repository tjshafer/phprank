<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateCountryKeyRule implements Rule
{
    public function __construct(public mixed $excludedCountries = null)
    {
        $this->excludedCountries = array_flip($excludedCountries);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if (array_key_exists($value, array_diff_key(config('countries'), $this->excludedCountries))) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Invalid country.');
    }
}
