<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateKeywordsCountRule implements Rule
{
    /**
     * The input attribute
     *
     * @var string
     */
    private $attribute;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;

        if (count(preg_split('/\n|\r/', $value, -1, PREG_SPLIT_NO_EMPTY)) > config('settings.ke_keywords')) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The number of :attribute must not exceed :value.', ['attribute' => $this->attribute, 'value' => config('settings.ke_keywords')]);
    }
}
