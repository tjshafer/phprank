<?php

namespace App\Providers;

use App\Http\View\Composers\FooterPagesComposer;
use App\Http\View\Composers\UserReportsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        View::composer('shared.footer', FooterPagesComposer::class);

        View::composer([
            'shared.header',
        ], UserReportsComposer::class);
    }
}
