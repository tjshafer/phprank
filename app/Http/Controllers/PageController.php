<?php

namespace App\Http\Controllers;

use App\Models\Page;

class PageController extends Controller
{
    /**
     * Show the page.
     *
     * @param $url
     */
    public function show($id): View
    {
        $page = Page::where('slug', $id)->firstOrFail();

        return view('pages.show', ['page' => $page]);
    }
}
