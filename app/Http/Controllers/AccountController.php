<?php

namespace App\Http\Controllers;

use App\Http\Requests\DestroyUserRequest;
use App\Http\Requests\UpdateUserPreferencesRequest;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Http\Requests\UpdateUserSecurityRequest;
use App\Models\Payment;
use App\Models\Plan;
use App\Traits\UserTrait;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AccountController extends Controller
{
    use UserTrait;

    /**
     * Show the Settings index.
     */
    public function index(Request $request): View
    {
        return view('account.container', ['view' => 'index', 'user' => $request->user()]);
    }

    /**
     * Show the Profile settings form.
     */
    public function profile(Request $request): View
    {
        return view('account.container', ['view' => 'profile', 'user' => $request->user()]);
    }

    /**
     * Update the Profile settings.
     *
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateProfile(UpdateUserProfileRequest $request): RedirectResponse
    {
        $this->userUpdate($request, $request->user());

        return redirect()->back()->with('success', __('Settings saved.'));
    }

    /**
     * Resent the Account Email Confirmation request.
     */
    public function resendAccountEmailConfirmation(Request $request): RedirectResponse
    {
        try {
            $request->user()->resendPendingEmailVerificationMail();
        } catch (\Exception $e) {
            return to_route('account.profile')->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success', __('A new verification link has been sent to your email address.'));
    }

    /**
     * Cancel the Account Email Confirmation request.
     */
    public function cancelAccountEmailConfirmation(Request $request): RedirectResponse
    {
        $request->user()->clearPendingEmail();

        return redirect()->back();
    }

    /**
     * Show the Security settings form.
     */
    public function security(Request $request): View
    {
        return view('account.container', ['view' => 'security', 'user' => $request->user()]);
    }

    /**
     * Update the Security settings.
     */
    public function updateSecurity(UpdateUserSecurityRequest $request): RedirectResponse
    {
        if ($request->input('password')) {
            $request->user()->password = Hash::make($request->input('password'));

            Auth::logoutOtherDevices($request->input('password'));
        }

        if ($request->has('tfa')) {
            $request->user()->tfa = $request->boolean('tfa');
        }

        $request->user()->save();

        return redirect()->back()->with('success', __('Settings saved.'));
    }

    /**
     * Show the Preference settings form.
     */
    public function preferences(Request $request): View
    {
        return view('account.container', ['view' => 'preferences']);
    }

    /**
     * Update the Preferences settings.
     */
    public function updatePreferences(UpdateUserPreferencesRequest $request): RedirectResponse
    {
        $request->user()->default_privacy = $request->input('default_privacy');
        $request->user()->default_export_detailed = $request->input('default_export_detailed');
        $request->user()->brand = [
            'logo' => $request->input('brand_logo'),
            'name' => $request->input('brand_name'),
            'url' => $request->input('brand_url'),
            'email' => $request->input('brand_email'),
            'phone' => $request->input('brand_phone'),
        ];
        $request->user()->save();

        return redirect()->back()->with('success', __('Settings saved.'));
    }

    /**
     * Show the Plan settings form.
     */
    public function plan(Request $request): View
    {
        return view('account.container', ['view' => 'plan', 'user' => $request->user()]);
    }

    /**
     * Update the Plan settings.
     */
    public function updatePlan(Request $request): RedirectResponse
    {
        $request->user()->planSubscriptionCancel();

        return redirect()->back()->with('success', __('Settings saved.'));
    }

    /**
     * List the Payments.
     */
    public function indexPayments(Request $request): View
    {
        $search = $request->input('search');
        $searchBy = in_array($request->input('search_by'), ['payment', 'invoice']) ? $request->input('search_by') : 'payment';
        $plan = $request->input('plan');
        $interval = $request->input('interval');
        $processor = $request->input('processor');
        $status = $request->input('status');
        $sortBy = in_array($request->input('sort_by'), ['id']) ? $request->input('sort_by') : 'id';
        $sort = in_array($request->input('sort'), ['asc', 'desc']) ? $request->input('sort') : 'desc';
        $perPage = in_array($request->input('per_page'), [10, 25, 50, 100]) ? $request->input('per_page') : config('settings.paginate');

        $payments = Payment::where('user_id', '=', $request->user()->id)
            ->when(isset($plan) && ! empty($plan), fn ($query) => $query->ofPlan($plan))
            ->when($interval, fn ($query) => $query->ofInterval($interval))
            ->when($processor, fn ($query) => $query->ofProcessor($processor))
            ->when($status, fn ($query) => $query->ofStatus($status))
            ->when($search, function ($query) use ($search, $searchBy) {
                if ($searchBy == 'invoice') {
                    return $query->searchInvoice($search);
                }

                return $query->searchPayment($search);
            })
            ->orderBy($sortBy, $sort)
            ->paginate($perPage)
            ->appends(['search' => $search, 'search_by' => $searchBy, 'interval' => $interval, 'processor' => $processor, 'plan' => $plan, 'status' => $status, 'sort_by' => $sortBy, 'sort' => $sort, 'per_page' => $perPage]);

        // Get all the plans
        $plans = Plan::query()->where([['amount_month', '>', 0], ['amount_year', '>', 0]])->withTrashed()->get();

        return view('account.container', ['view' => 'payments.list', 'payments' => $payments, 'plans' => $plans]);
    }

    /**
     * Show the edit Payment form.
     */
    public function editPayment(Request $request, int $id): View
    {
        $payment = Payment::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        return view('account.container', ['view' => 'payments.edit', 'payment' => $payment]);
    }

    /**
     * Cancel the Payment.
     */
    public function cancelPayment(Request $request, $id): RedirectResponse
    {
        $payment = Payment::where([['id', '=', $id], ['status', '=', 'pending'], ['user_id', '=', $request->user()->id]])->firstOrFail();
        $payment->status = 'cancelled';
        $payment->save();

        return to_route('account.payments.edit', $id)->with('success', __('Settings saved.'));
    }

    /**
     * Show the Invoice.
     */
    public function showInvoice(Request $request, $id): View
    {
        $payment = Payment::where([['user_id', '=', $request->user()->id], ['id', '=', $id], ['status', '=', 'completed']])->firstOrFail();

        // Sum the inclusive tax rates
        $inclTaxRatesPercentage = collect($payment->tax_rates)->where('type', '=', 0)->sum('percentage');

        // Sum the exclusive tax rates
        $exclTaxRatesPercentage = collect($payment->tax_rates)->where('type', '=', 1)->sum('percentage');

        return view('account.container', ['view' => 'payments.invoice', 'payment' => $payment, 'inclTaxRatesPercentage' => $inclTaxRatesPercentage, 'exclTaxRatesPercentage' => $exclTaxRatesPercentage]);
    }

    /**
     * Show the API settings form.
     */
    public function api(Request $request): View
    {
        return view('account.container', ['view' => 'api', 'user' => $request->user()]);
    }

    /**
     * Update the API settings.
     */
    public function updateApi(Request $request): RedirectResponse
    {
        $request->user()->api_token = Str::random(64);
        $request->user()->save();

        return redirect()->back()->with('success', __('Settings saved.'));
    }

    /**
     * Show the Delete Account form.
     */
    public function delete(Request $request): View
    {
        return view('account.container', ['view' => 'delete', 'user' => $request->user()]);
    }

    /**
     * Delete the Account.
     */
    public function destroyUser(DestroyUserRequest $request): RedirectResponse
    {
        $request->user()->forceDelete();

        return to_route('home');
    }
}
