<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use Illuminate\Contracts\View\View;

class PricingController extends Controller
{
    /**
     * Show the Pricing page.
     */
    public function index(): View
    {
        $plans = Plan::query()->where('visibility', 1)->oldest('position')->oldest('id')->get();

        return view('pricing.index', ['plans' => $plans]);
    }
}
