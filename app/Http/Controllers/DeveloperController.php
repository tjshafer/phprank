<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class DeveloperController extends Controller
{
    /**
     * Show the Developer index page.
     */
    public function index(): View
    {
        return view('developers.index');
    }

    /**
     * Show the Developer Projects page.
     */
    public function projects(): View
    {
        return view('developers.projects.index');
    }

    /**
     * Show the Developer Reports page.
     */
    public function reports(): View
    {
        return view('developers.reports.index');
    }

    /**
     * Show the Developer Account page.
     */
    public function account(): View
    {
        return view('developers.account.index');
    }
}
