<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreReportRequest;
use App\Http\Requests\UpdateReportRequest;
use App\Http\Requests\ValidateReportPasswordRequest;
use App\Models\Report;
use App\Traits\ReportTrait;
use Carbon\CarbonTimeZone;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Csv as CSV;

class ReportController extends Controller
{
    use ReportTrait;

    /**
     * List the Reports.
     */
    public function index(Request $request): View
    {
        // If there's no toast notification
        if ($request->session()->get('toast') == false) {
            // Set the session to a countable object
            $request->session()->put('toast', []);
        }

        $search = $request->input('search');
        $searchBy = in_array($request->input('search_by'), ['url']) ? $request->input('search_by') : 'url';
        $project = $request->input('project');
        $result = $request->input('result');
        $sortBy = in_array($request->input('sort_by'), ['id', 'generated_at', 'url', 'result']) ? $request->input('sort_by') : 'id';
        $sort = in_array($request->input('sort'), ['asc', 'desc']) ? $request->input('sort') : 'desc';
        $perPage = in_array($request->input('per_page'), [10, 25, 50, 100]) ? $request->input('per_page') : config('settings.paginate');

        $reports = Report::where('user_id', $request->user()->id)
            ->when($search, fn ($query) => $query->searchUrl($search))
            ->when($project, fn ($query) => $query->ofProject($project))
            ->when($result, fn ($query) => $query->ofResult($result))
            ->orderBy($sortBy, $sort)
            ->paginate($perPage)
            ->appends(['search' => $search, 'search_by' => $searchBy, 'project' => $project, 'result' => $result, 'sort_by' => $sortBy, 'sort' => $sort, 'per_page' => $perPage]);

        $projects = Report::select('project')->where('user_id', $request->user()->id)->groupBy('project')->oldest('project')->get(['project']);

        return view('reports.list', ['reports' => $reports, 'projects' => $projects]);
    }

    /**
     * Show the Report.
     */
    public function show(Request $request, int $id): View
    {
        $report = Report::where('id', '=', $id)->firstOrFail();

        if ($this->reportGuard($report)) {
            return view('reports.password', ['report' => $report]);
        }

        return view('reports.container', ['view' => 'show', 'report' => $report]);
    }

    /**
     * Show the edit Report form.
     */
    public function edit(Request $request, int $id): View
    {
        $report = Report::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        return view('reports.container', ['view' => 'edit', 'report' => $report]);
    }

    /**
     * Store the Report.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(StoreReportRequest $request): RedirectResponse
    {
        if ($request->input('sitemap') && config('settings.sitemap_links') != 0) {
            $reports = $this->reportsStore($request);

            return redirect()->back()->with('toast', Report::where('user_id', '=', $request->user()->id)->latest('id')->limit(count($reports))->get());
        }
        $this->reportStore($request);

        return redirect()->back()->with('toast', Report::where('user_id', '=', $request->user()->id)->latest('id')->limit(1)->get());
    }

    /**
     * Update the Report.
     *
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(UpdateReportRequest $request, int $id): RedirectResponse
    {
        $report = Report::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        $this->reportUpdate($request, $report);

        return redirect()->back()->with('success', __('Settings saved.'));
    }

    /**
     * Delete the Report.
     *
     * @throws \Exception
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $report = Report::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        $report->delete();

        return to_route('reports', ['project' => request()->input('project')])->with('success', __(':name has been deleted.', ['name' => $report->url]));
    }

    /**
     * Export the Reports.
     *
     * @throws CSV\CannotInsertRecord
     */
    public function export(Request $request): Response
    {
        $search = $request->input('search');
        $searchBy = in_array($request->input('search_by'), ['url']) ? $request->input('search_by') : 'url';
        $project = $request->input('project');
        $result = $request->input('result');
        $sortBy = in_array($request->input('sort_by'), ['id', 'generated_at', 'url', 'result']) ? $request->input('sort_by') : 'id';
        $sort = in_array($request->input('sort'), ['asc', 'desc']) ? $request->input('sort') : 'desc';

        $reports = Report::where('user_id', $request->user()->id)
            ->when($search, fn ($query) => $query->searchUrl($search))
            ->when($project, fn ($query) => $query->ofProject($project))
            ->when($result, fn ($query) => $query->ofResult($result))
            ->orderBy($sortBy, $sort)
            ->get();

        $content = CSV\Writer::createFromFileObject(new \SplTempFileObject);

        // Generate the header
        $content->insertOne([__('Type'), __('Reports')]);
        $content->insertOne([__('Date'), Carbon::now()->tz($request->user()->timezone ?? config('app.timezone'))->format(__('Y-m-d')).' '.Carbon::now()->tz($request->user()->timezone ?? config('app.timezone'))->format('H:i:s').' ('.CarbonTimeZone::create($request->user()->timezone ?? config('app.timezone'))->toOffsetName().')']);
        $content->insertOne([__('URL'), $request->fullUrl()]);
        $content->insertOne([__(' ')]);

        // Generate the content
        $content->insertOne([__('URL'), __('Result'), __('High issues'), __('Medium issues'), __('Low issues'), __('Generated at'), __('Created at')]);
        foreach ($reports as $report) {
            $content->insertOne([$report->url, $report->result, $report->highIssuesCount, $report->mediumIssuesCount, $report->lowIssuesCount, $report->generated_at->tz($request->user()->timezone ?? config('app.timezone'))->format(__('Y-m-d')), $report->created_at->tz($request->user()->timezone ?? config('app.timezone'))->format(__('Y-m-d'))]);
        }

        return response([
            'Content-Type' => 'text/csv',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="'.formatTitle(($project ? [e($request->input('project')), __('Reports'), config('settings.title')] : [__('Reports'), config('settings.title')])).'.csv"',
        ]);
    }

    /**
     * Validate the Report's password.
     */
    public function validatePassword(ValidateReportPasswordRequest $request, int $id): RedirectResponse
    {
        session([md5($id) => true]);

        return redirect()->back();
    }

    /**
     * Guard the Report page.
     */
    private function reportGuard(Report $report): bool
    {
        // If the link stats is not set to public
        if ($report->privacy !== 0) {
            $user = Auth::user();

            // If the website's privacy is set to private
            if ($report->privacy == 1) {
                // If the user is not authenticated
                // Or if the user is not the owner of the link and not an admin
                if ($user == null || $user->id != $report->user_id && $user->role != 1) {
                    abort(403);
                }
            }

            // If the website's privacy is set to password
            if ($report->privacy == 2) {
                // If there's no password validation in the current session
                if (! session(md5($report->id))) {
                    // If the user is not authenticated
                    // Or if the user is not the owner of the link and not an admin
                    if ($user == null || $user->id != $report->user_id && $user->role != 1) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
