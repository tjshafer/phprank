<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\StoreReportRequest;
use App\Http\Requests\API\UpdateReportRequest;
use App\Http\Resources\ReportResource;
use App\Models\Report;
use App\Traits\ReportTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ReportController extends Controller
{
    use ReportTrait;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $search = $request->input('search');
        $searchBy = in_array($request->input('search_by'), ['url']) ? $request->input('search_by') : 'url';
        $project = $request->input('project');
        $result = $request->input('result');
        $sortBy = in_array($request->input('sort_by'), ['id', 'url', 'result']) ? $request->input('sort_by') : 'id';
        $sort = in_array($request->input('sort'), ['asc', 'desc']) ? $request->input('sort') : 'desc';
        $perPage = in_array($request->input('per_page'), [10, 25, 50, 100]) ? $request->input('per_page') : config('settings.paginate');

        return ReportResource::collection(Report::where('user_id', $request->user()->id)
            ->when($search, fn ($query) => $query->searchUrl($search))
            ->when($project, fn ($query) => $query->ofProject($project))
            ->when($result, fn ($query) => $query->ofResult($result))
            ->orderBy($sortBy, $sort)
            ->paginate($perPage)
            ->appends(['search' => $search, 'search_by' => $searchBy, 'project' => $project, 'result' => $result, 'sort_by' => $sortBy, 'sort' => $sort, 'per_page' => $perPage]))
            ->additional(['status' => 200]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreReportRequest $request): ReportResource|JsonResponse
    {
        $created = $this->reportStore($request);

        if ($created) {
            return ReportResource::make($created);
        }

        return response()->json([
            'message' => __('Resource not found.'),
            'status' => 404,
        ], 404);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, int $id): ReportResource|JsonResponse
    {
        $link = Report::where([['id', '=', $id], ['user_id', $request->user()->id]])->first();

        if ($link) {
            return ReportResource::make($link);
        }

        return response()->json([
            'message' => __('Resource not found.'),
            'status' => 404,
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateReportRequest $request, int $id): ReportResource|JsonResponse
    {
        $report = Report::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        $updated = $this->reportUpdate($request, $report);

        if ($updated) {
            return ReportResource::make($updated);
        }

        return response()->json([
            'message' => __('Resource not found.'),
            'status' => 404,
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @throws \Exception
     */
    public function destroy(Request $request, int $id): JsonResponse
    {
        $report = Report::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->first();

        if ($report) {
            $report->delete();

            return response()->json([
                'id' => $report->id,
                'object' => 'report',
                'deleted' => true,
                'status' => 200,
            ], 200);
        }

        return response()->json([
            'message' => __('Resource not found.'),
            'status' => 404,
        ], 404);
    }
}
