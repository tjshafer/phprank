<?php

namespace App\Http\Requests;

use App\Rules\ValidateKeywordsCountRule;
use Illuminate\Foundation\Http\FormRequest;

class ProcessKeywordResearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'keywords' => ['required', 'string', 'min:1', 'max:2048', new ValidateKeywordsCountRule()],
            'country' => ['nullable', 'string'],
            'currency' => ['nullable', 'string'],
        ];
    }
}
