<?php

namespace App\Http\Requests;

use App\Rules\ValidateDomainNameRule;
use Illuminate\Foundation\Http\FormRequest;

class ProcessWhoisLookupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'domain' => ['required', 'string', 'max:2048', new ValidateDomainNameRule()],
        ];
    }
}
