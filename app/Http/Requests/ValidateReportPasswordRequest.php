<?php

namespace App\Http\Requests;

use App\Models\Report;
use App\Rules\ValidateReportPasswordRule;
use Illuminate\Foundation\Http\FormRequest;

class ValidateReportPasswordRequest extends FormRequest
{
    /**
     * @var
     */
    public $report;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $this->report = Report::where('id', $this->route('id'))->firstOrFail();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'password' => ['required', new ValidateReportPasswordRule($this->report)],
        ];
    }
}
