<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CronjobController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DeveloperController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LocaleController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PricingController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ToolController;
use App\Http\Controllers\WebhookController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth routes
Auth::routes(['verify' => true]);
Route::post('login/tfa/validate', [Auth\LoginController::class, 'validateTfaCode'])->name('login.tfa.validate');
Route::post('login/tfa/resend', [Auth\LoginController::class, 'resendTfaCode'])->name('login.tfa.resend');

// Locale routes
Route::post('/locale', [LocaleController::class, 'updateLocale'])->name('locale');

// Home routes
Route::get('/', [HomeController::class, 'index'])->name('home');

// Contact routes
Route::get('/contact', [ContactController::class, 'index'])->name('contact');
Route::post('/contact', [ContactController::class, 'send'])->middleware('throttle:5,10');

// Page routes
Route::get('/pages/{id}', [PageController::class, 'show'])->name('pages.show');

// Dashboard routes
Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('verified')->name('dashboard');

// Report routes
Route::get('/reports', [ReportController::class, 'index'])->middleware('verified')->name('reports');
Route::get('/reports/export', [ReportController::class, 'export'])->middleware('verified')->name('reports.export');
Route::get('/reports/{id}', [ReportController::class, 'show'])->name('reports.show');
Route::get('/reports/{id}/edit', [ReportController::class, 'edit'])->middleware('verified')->name('reports.edit');
Route::post('/reports/new', [ReportController::class, 'store'])->name('reports.new');
Route::post('/reports/{id}/edit', [ReportController::class, 'update']);
Route::post('/reports/{id}/destroy', [ReportController::class, 'destroy'])->name('reports.destroy');
Route::post('/reports/{id}/password', [ReportController::class, 'validatePassword'])->name('reports.password');

// Project routes
Route::get('/projects', [ProjectController::class, 'index'])->middleware('verified')->name('projects');
Route::get('/projects/export', [ProjectController::class, 'export'])->middleware('verified')->name('projects.export');
Route::post('/projects/{project}/destroy', [ProjectController::class, 'destroy'])->middleware('verified')->name('projects.destroy');

// Tool routes
Route::prefix('tools')->middleware('tools')->group(function () {
    Route::get('/', [ToolController::class, 'index'])->name('tools');

    Route::get('/serp-checker', [ToolController::class, 'serpChecker'])->name('tools.serp_checker');
    Route::post('/serp-checker', [ToolController::class, 'processSerpChecker'])->middleware('throttle:200,1440');

    Route::get('/indexed-pages-checker', [ToolController::class, 'indexedPagesChecker'])->name('tools.indexed_pages_checker');
    Route::post('/indexed-pages-checker', [ToolController::class, 'processIndexedPagesChecker'])->middleware('throttle:200,1440');

    Route::get('/keyword-research', [ToolController::class, 'keywordResearch'])->name('tools.keyword_research');
    Route::post('/keyword-research', [ToolController::class, 'processKeywordResearch'])->middleware('throttle:200,1440');

    Route::get('/website-status-checker', [ToolController::class, 'websiteStatusChecker'])->name('tools.website_status_checker');
    Route::post('/website-status-checker', [ToolController::class, 'processWebsiteStatusChecker']);

    Route::get('/ssl-checker', [ToolController::class, 'sslChecker'])->name('tools.ssl_checker');
    Route::post('/ssl-checker', [ToolController::class, 'processSslChecker']);

    Route::get('/dns-lookup', [ToolController::class, 'dnsLookup'])->name('tools.dns_lookup');
    Route::post('/dns-lookup', [ToolController::class, 'processDnsLookup']);

    Route::get('/whois-lookup', [ToolController::class, 'whoisLookup'])->name('tools.whois_lookup');
    Route::post('/whois-lookup', [ToolController::class, 'processWhoisLookup']);

    Route::get('/ip-lookup', [ToolController::class, 'ipLookup'])->name('tools.ip_lookup');
    Route::post('/ip-lookup', [ToolController::class, 'processIpLookup']);

    Route::get('/reverse-ip-lookup', [ToolController::class, 'reverseIpLookup'])->name('tools.reverse_ip_lookup');
    Route::post('/reverse-ip-lookup', [ToolController::class, 'processReverseIpLookup']);

    Route::get('/domain-ip-lookup', [ToolController::class, 'domainIpLookup'])->name('tools.domain_ip_lookup');
    Route::post('/domain-ip-lookup', [ToolController::class, 'processDomainIpLookup']);

    Route::get('/redirect-checker', [ToolController::class, 'redirectChecker'])->name('tools.redirect_checker');
    Route::post('/redirect-checker', [ToolController::class, 'processRedirectChecker']);

    Route::get('/idn-converter', [ToolController::class, 'idnConverter'])->name('tools.idn_converter');
    Route::post('/idn-converter', [ToolController::class, 'processIdnConverter']);

    Route::get('/js-minifier', [ToolController::class, 'jsMinifier'])->name('tools.js_minifier');
    Route::post('/js-minifier', [ToolController::class, 'processJsMinifier']);

    Route::get('/css-minifier', [ToolController::class, 'cssMinifier'])->name('tools.css_minifier');
    Route::post('/css-minifier', [ToolController::class, 'processCssMinifier']);

    Route::get('/html-minifier', [ToolController::class, 'htmlMinifier'])->name('tools.html_minifier');
    Route::post('/html-minifier', [ToolController::class, 'processHtmlMinifier']);

    Route::get('/json-validator', [ToolController::class, 'jsonValidator'])->name('tools.json_validator');
    Route::post('/json-validator', [ToolController::class, 'processJsonValidator']);

    Route::get('/password-generator', [ToolController::class, 'passwordGenerator'])->name('tools.password_generator');
    Route::post('/password-generator', [ToolController::class, 'processPasswordGenerator']);

    Route::get('/qr-generator', [ToolController::class, 'qrGenerator'])->name('tools.qr_generator');
    Route::post('/qr-generator', [ToolController::class, 'processQrGenerator']);

    Route::get('/user-agent-parser', [ToolController::class, 'userAgentParser'])->name('tools.user_agent_parser');
    Route::post('/user-agent-parser', [ToolController::class, 'processUserAgentParser']);

    Route::get('/md5-generator', [ToolController::class, 'md5Generator'])->name('tools.md5_generator');
    Route::post('/md5-generator', [ToolController::class, 'processMd5Generator']);

    Route::get('/color-converter', [ToolController::class, 'colorConverter'])->name('tools.color_converter');
    Route::post('/color-converter', [ToolController::class, 'processColorConverter']);

    Route::get('/utm-builder', [ToolController::class, 'utmBuilder'])->name('tools.utm_builder');
    Route::post('/utm-builder', [ToolController::class, 'processUtmBuilder']);

    Route::get('/url-parser', [ToolController::class, 'urlParser'])->name('tools.url_parser');
    Route::post('/url-parser', [ToolController::class, 'processUrlParser']);

    Route::get('/uuid-generator', [ToolController::class, 'uuidGenerator'])->name('tools.uuid_generator');
    Route::post('/uuid-generator', [ToolController::class, 'processUuidGenerator']);

    Route::get('/lorem-ipsum-generator', [ToolController::class, 'loremIpsumGenerator'])->name('tools.lorem_ipsum_generator');
    Route::post('/lorem-ipsum-generator', [ToolController::class, 'processLoremIpsumGenerator']);

    Route::get('/text-cleaner', [ToolController::class, 'textCleaner'])->name('tools.text_cleaner');
    Route::post('/text-cleaner', [ToolController::class, 'processTextCleaner']);

    Route::get('/word-density-counter', [ToolController::class, 'wordDensityCounter'])->name('tools.word_density_counter');
    Route::post('/word-density-counter', [ToolController::class, 'processWordDensityCounter']);

    Route::get('/word-counter', [ToolController::class, 'wordCounter'])->name('tools.word_counter');
    Route::post('/word-counter', [ToolController::class, 'processWordCounter']);

    Route::get('/case-converter', [ToolController::class, 'caseConverter'])->name('tools.case_converter');
    Route::post('/case-converter', [ToolController::class, 'processCaseConverter']);

    Route::get('/text-to-slug-converter', [ToolController::class, 'textToSlugConverter'])->name('tools.text_to_slug_converter');
    Route::post('/text-to-slug-converter', [ToolController::class, 'processTextToSlugConverter']);

    Route::get('/url-converter', [ToolController::class, 'urlConverter'])->name('tools.url_converter');
    Route::post('/url-converter', [ToolController::class, 'processUrlConverter']);

    Route::get('/base64-converter', [ToolController::class, 'base64Converter'])->name('tools.base64_converter');
    Route::post('/base64-converter', [ToolController::class, 'processBase64Converter']);

    Route::get('/binary-converter', [ToolController::class, 'binaryConverter'])->name('tools.binary_converter');
    Route::post('/binary-converter', [ToolController::class, 'processBinaryConverter']);

    Route::get('/text-replacer', [ToolController::class, 'textReplacer'])->name('tools.text_replacer');
    Route::post('/text-replacer', [ToolController::class, 'processTextReplacer']);

    Route::get('/text-reverser', [ToolController::class, 'textReverser'])->name('tools.text_reverser');
    Route::post('/text-reverser', [ToolController::class, 'processTextReverser']);

    Route::get('/number-generator', [ToolController::class, 'numberGenerator'])->name('tools.number_generator');
    Route::post('/number-generator', [ToolController::class, 'processNumberGenerator']);
});

// Account routes
Route::prefix('account')->middleware('verified')->group(function () {
    Route::get('/', [AccountController::class, 'index'])->name('account');

    Route::get('/profile', [AccountController::class, 'profile'])->name('account.profile');
    Route::post('/profile', [AccountController::class, 'updateProfile'])->name('account.profile.update');
    Route::post('/profile/resend', [AccountController::class, 'resendAccountEmailConfirmation'])->name('account.profile.resend');
    Route::post('/profile/cancel', [AccountController::class, 'cancelAccountEmailConfirmation'])->name('account.profile.cancel');

    Route::get('/security', [AccountController::class, 'security'])->name('account.security');
    Route::post('/security', [AccountController::class, 'updateSecurity']);

    Route::get('/preferences', [AccountController::class, 'preferences'])->name('account.preferences');
    Route::post('/preferences', [AccountController::class, 'updatePreferences']);

    Route::get('/plan', [AccountController::class, 'plan'])->middleware('payment')->name('account.plan');
    Route::post('/plan', [AccountController::class, 'updatePlan'])->middleware('payment');

    Route::get('/payments', [AccountController::class, 'indexPayments'])->middleware('payment')->name('account.payments');
    Route::get('/payments/{id}/edit', [AccountController::class, 'editPayment'])->middleware('payment')->name('account.payments.edit');
    Route::post('/payments/{id}/cancel', [AccountController::class, 'cancelPayment'])->name('account.payments.cancel');

    Route::get('/invoices/{id}', [AccountController::class, 'showInvoice'])->middleware('payment')->name('account.invoices.show');

    Route::get('/api', [AccountController::class, 'api'])->name('account.api');
    Route::post('/api', [AccountController::class, 'updateApi']);

    Route::get('/delete', [AccountController::class, 'delete'])->name('account.delete');
    Route::post('/destroy', [AccountController::class, 'destroyUser'])->name('account.destroy');
});

// Admin routes
Route::prefix('admin')->middleware('admin')->group(function () {
    Route::redirect('/', 'admin/dashboard');

    Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');

    Route::get('/settings/{id}', [AdminController::class, 'settings'])->name('admin.settings');
    Route::post('/settings/{id}', [AdminController::class, 'updateSetting']);

    Route::get('/users', [AdminController::class, 'indexUsers'])->name('admin.users');
    Route::get('/users/new', [AdminController::class, 'createUser'])->name('admin.users.new');
    Route::get('/users/{id}/edit', [AdminController::class, 'editUser'])->name('admin.users.edit');
    Route::post('/users/new', [AdminController::class, 'storeUser']);
    Route::post('/users/{id}/edit', [AdminController::class, 'updateUser']);
    Route::post('/users/{id}/destroy', [AdminController::class, 'destroyUser'])->name('admin.users.destroy');
    Route::post('/users/{id}/disable', [AdminController::class, 'disableUser'])->name('admin.users.disable');
    Route::post('/users/{id}/restore', [AdminController::class, 'restoreUser'])->name('admin.users.restore');

    Route::get('/pages', [AdminController::class, 'indexPages'])->name('admin.pages');
    Route::get('/pages/new', [AdminController::class, 'createPage'])->name('admin.pages.new');
    Route::get('/pages/{id}/edit', [AdminController::class, 'editPage'])->name('admin.pages.edit');
    Route::post('/pages/new', [AdminController::class, 'storePage']);
    Route::post('/pages/{id}/edit', [AdminController::class, 'updatePage']);
    Route::post('/pages/{id}/destroy', [AdminController::class, 'destroyPage'])->name('admin.pages.destroy');

    Route::get('/payments', [AdminController::class, 'indexPayments'])->name('admin.payments');
    Route::get('/payments/{id}/edit', [AdminController::class, 'editPayment'])->name('admin.payments.edit');
    Route::post('/payments/{id}/approve', [AdminController::class, 'approvePayment'])->name('admin.payments.approve');
    Route::post('/payments/{id}/cancel', [AdminController::class, 'cancelPayment'])->name('admin.payments.cancel');

    Route::get('/invoices/{id}', [AdminController::class, 'showInvoice'])->name('admin.invoices.show');

    Route::get('/plans', [AdminController::class, 'indexPlans'])->name('admin.plans');
    Route::get('/plans/new', [AdminController::class, 'createPlan'])->name('admin.plans.new');
    Route::get('/plans/{id}/edit', [AdminController::class, 'editPlan'])->name('admin.plans.edit');
    Route::post('/plans/new', [AdminController::class, 'storePlan']);
    Route::post('/plans/{id}/edit', [AdminController::class, 'updatePlan']);
    Route::post('/plans/{id}/disable', [AdminController::class, 'disablePlan'])->name('admin.plans.disable');
    Route::post('/plans/{id}/restore', [AdminController::class, 'restorePlan'])->name('admin.plans.restore');

    Route::get('/coupons', [AdminController::class, 'indexCoupons'])->name('admin.coupons');
    Route::get('/coupons/new', [AdminController::class, 'createCoupon'])->name('admin.coupons.new');
    Route::get('/coupons/{id}/edit', [AdminController::class, 'editCoupon'])->name('admin.coupons.edit');
    Route::post('/coupons/new', [AdminController::class, 'storeCoupon']);
    Route::post('/coupons/{id}/edit', [AdminController::class, 'updateCoupon']);
    Route::post('/coupons/{id}/disable', [AdminController::class, 'disableCoupon'])->name('admin.coupons.disable');
    Route::post('/coupons/{id}/restore', [AdminController::class, 'restoreCoupon'])->name('admin.coupons.restore');

    Route::get('/tax-rates', [AdminController::class, 'indexTaxRates'])->name('admin.tax_rates');
    Route::get('/tax-rates/new', [AdminController::class, 'createTaxRate'])->name('admin.tax_rates.new');
    Route::get('/tax-rates/{id}/edit', [AdminController::class, 'editTaxRate'])->name('admin.tax_rates.edit');
    Route::post('/tax-rates/new', [AdminController::class, 'storeTaxRate']);
    Route::post('/tax-rates/{id}/edit', [AdminController::class, 'updateTaxRate']);
    Route::post('/tax-rates/{id}/disable', [AdminController::class, 'disableTaxRate'])->name('admin.tax_rates.disable');
    Route::post('/tax-rates/{id}/restore', [AdminController::class, 'restoreTaxRate'])->name('admin.tax_rates.restore');

    Route::get('/reports', [AdminController::class, 'indexReports'])->name('admin.reports');
    Route::get('/reports/{id}/edit', [AdminController::class, 'editReport'])->name('admin.reports.edit');
    Route::post('/reports/{id}/edit', [AdminController::class, 'updateReport']);
    Route::post('/reports/{id}/destroy', [AdminController::class, 'destroyReport'])->name('admin.reports.destroy');
});

// Pricing routes
Route::prefix('pricing')->middleware('payment')->group(function () {
    Route::get('/', [PricingController::class, 'index'])->name('pricing');
});

// Checkout routes
Route::prefix('checkout')->middleware('verified', 'payment')->group(function () {
    Route::get('/cancelled', [CheckoutController::class, 'cancelled'])->name('checkout.cancelled');
    Route::get('/pending', [CheckoutController::class, 'pending'])->name('checkout.pending');
    Route::get('/complete', [CheckoutController::class, 'complete'])->name('checkout.complete');

    Route::get('/{id}', [CheckoutController::class, 'index'])->name('checkout.index');
    Route::post('/{id}', [CheckoutController::class, 'process']);
});

// Cronjob routes
Route::get('/cronjob', [CronjobController::class, 'index'])->name('cronjob');

// Webhook routes
Route::post('webhooks/paypal', [WebhookController::class, 'paypal'])->name('webhooks.paypal');
Route::post('webhooks/stripe', [WebhookController::class, 'stripe'])->name('webhooks.stripe');
Route::post('webhooks/razorpay', [WebhookController::class, 'razorpay'])->name('webhooks.razorpay');
Route::post('webhooks/paystack', [WebhookController::class, 'paystack'])->name('webhooks.paystack');
Route::post('webhooks/cryptocom', [WebhookController::class, 'cryptocom'])->name('webhooks.cryptocom');
Route::post('webhooks/coinbase', [WebhookController::class, 'coinbase'])->name('webhooks.coinbase');

// Developer routes
Route::prefix('/developers')->group(function () {
    Route::get('/', [DeveloperController::class, 'index'])->name('developers');
    Route::get('/projects', [DeveloperController::class, 'projects'])->name('developers.projects');
    Route::get('/reports', [DeveloperController::class, 'reports'])->name('developers.reports');
    Route::get('/account', [DeveloperController::class, 'account'])->name('developers.account');
});
