<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->string('name', 128)->primary();
            $table->text('value')->nullable();
        });

        DB::table('settings')->insert(
            [
                ['name' => 'announcement_guest', 'value' => null],
                ['name' => 'announcement_guest_id', 'value' => 'PORn7QrVMarbZGmr'],
                ['name' => 'announcement_guest_type', 'value' => 'primary'],
                ['name' => 'announcement_user', 'value' => null],
                ['name' => 'announcement_user_id', 'value' => 'Wu5yiu5Vj1cjmJvy'],
                ['name' => 'announcement_user_type', 'value' => 'danger'],
                ['name' => 'bank', 'value' => '0'],
                ['name' => 'bank_account_number', 'value' => null],
                ['name' => 'bank_account_owner', 'value' => null],
                ['name' => 'bank_bic_swift', 'value' => null],
                ['name' => 'bank_iban', 'value' => null],
                ['name' => 'bank_name', 'value' => null],
                ['name' => 'bank_routing_number', 'value' => null],
                ['name' => 'billing_address', 'value' => ''],
                ['name' => 'billing_city', 'value' => ''],
                ['name' => 'billing_country', 'value' => ''],
                ['name' => 'billing_invoice_prefix', 'value' => ''],
                ['name' => 'billing_phone', 'value' => ''],
                ['name' => 'billing_postal_code', 'value' => ''],
                ['name' => 'billing_state', 'value' => ''],
                ['name' => 'billing_vat_number', 'value' => ''],
                ['name' => 'coinbase', 'value' => '0'],
                ['name' => 'coinbase_key', 'value' => null],
                ['name' => 'coinbase_wh_secret', 'value' => null],
                ['name' => 'contact_email', 'value' => null],
                ['name' => 'cronjob_key', 'value' => Str::random(32)],
                ['name' => 'custom_css', 'value' => '@import url("https://rsms.me/inter/inter.css");'],
                ['name' => 'demo_url', 'value' => ''],
                ['name' => 'email_address', 'value' => null],
                ['name' => 'email_driver', 'value' => 'log'],
                ['name' => 'email_encryption', 'value' => 'log'],
                ['name' => 'email_host', 'value' => null],
                ['name' => 'email_password', 'value' => null],
                ['name' => 'email_port', 'value' => null],
                ['name' => 'email_username', 'value' => null],
                ['name' => 'favicon', 'value' => 'favicon.png'],
                ['name' => 'index', 'value' => null],
                ['name' => 'legal_cookie_url', 'value' => null],
                ['name' => 'legal_privacy_url', 'value' => null],
                ['name' => 'legal_terms_url', 'value' => null],
                ['name' => 'locale', 'value' => 'en'],
                ['name' => 'logo', 'value' => 'logo.svg'],
                ['name' => 'paginate', 'value' => '10'],
                ['name' => 'paypal', 'value' => '0'],
                ['name' => 'paypal_client_id', 'value' => null],
                ['name' => 'paypal_mode', 'value' => 'sandbox'],
                ['name' => 'paypal_secret', 'value' => null],
                ['name' => 'paypal_webhook_id', 'value' => null],
                ['name' => 'registration', 'value' => '1'],
                ['name' => 'registration_verification', 'value' => '1'],
                ['name' => 'report_bad_words', 'value' => null],
                ['name' => 'report_connection_timeout', 'value' => '5'],
                ['name' => 'report_gsb', 'value' => '0'],
                ['name' => 'report_gsb_key', 'value' => null],
                ['name' => 'report_limit_deprecated_html_tags', 'value' => 'acronym
applet
basefont
big
center
dir
font
frame
frameset
isindex
noframes
s
strike
tt
u'],
                ['name' => 'report_limit_http_requests', 'value' => '50'],
                ['name' => 'report_limit_image_formats', 'value' => 'AVIF
WebP'],
                ['name' => 'report_limit_load_time', 'value' => '2'],
                ['name' => 'report_limit_max_dom_nodes', 'value' => '1500'],
                ['name' => 'report_limit_max_links', 'value' => '150'],
                ['name' => 'report_limit_max_title', 'value' => '60'],
                ['name' => 'report_limit_min_text_ratio', 'value' => '10'],
                ['name' => 'report_limit_min_title', 'value' => '1'],
                ['name' => 'report_limit_min_words', 'value' => '500'],
                ['name' => 'report_limit_page_size', 'value' => '330000'],
                ['name' => 'report_score_high', 'value' => '10'],
                ['name' => 'report_score_low', 'value' => '0'],
                ['name' => 'report_score_medium', 'value' => '5'],
                ['name' => 'report_screenshot', 'value' => '0'],
                ['name' => 'report_screenshot_key', 'value' => null],
                ['name' => 'social_facebook', 'value' => null],
                ['name' => 'social_instagram', 'value' => null],
                ['name' => 'social_twitter', 'value' => null],
                ['name' => 'social_youtube', 'value' => null],
                ['name' => 'stripe', 'value' => '0'],
                ['name' => 'stripe_key', 'value' => null],
                ['name' => 'stripe_secret', 'value' => null],
                ['name' => 'stripe_wh_secret', 'value' => null],
                ['name' => 'tagline', 'value' => 'Insightful and concise SEO reports'],
                ['name' => 'theme', 'value' => '0'],
                ['name' => 'timezone', 'value' => 'UTC'],
                ['name' => 'title', 'value' => 'phpRank'],
                ['name' => 'tracking_code', 'value' => null],
            ]
        );
    }
};
