<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::table('settings')->insert(
            [
                ['name' => 'gcs_country', 'value' => 'US'],
                ['name' => 'sitemap_links', 'value' => '-1'],
                ['name' => 'request_proxy', 'value' => null],
            ]
        );
    }
};
