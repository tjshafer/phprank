<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $settings = array_combine(['custom_js', 'bad_words', 'request_connection_timeout', 'gsb', 'gsb_key', 'screenshot', 'screenshot_key'], ['tracking_code', 'report_bad_words', 'report_connection_timeout', 'report_gsb', 'report_gsb_key', 'report_screenshot', 'report_screenshot_key']);

        $sqlQuery = null;
        foreach ($settings as $new => $old) {
            $sqlQuery .= "WHEN `name` = '".$old."' THEN '".$new."' ";
        }

        DB::update('UPDATE `settings` SET `name` = CASE '.$sqlQuery." END WHERE `name` IN ('".implode("', '", $settings)."')");

        DB::table('settings')->insert(
            [
                ['name' => 'request_user_agent', 'value' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'],
                ['name' => 'gcs', 'value' => null],
                ['name' => 'gcs_key', 'value' => null],
                ['name' => 'gcs_id', 'value' => null],
            ]
        );
    }
};
