<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tax_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index('name');
            $table->tinyInteger('type')->index('type');
            $table->decimal('percentage', 5);
            $table->text('regions')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
